import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import './styles/css/index.css'
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import store from './js/store.js'
import $ from 'jquery'


ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
registerServiceWorker();
