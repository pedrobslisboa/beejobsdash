import React, { Component } from 'react'
import NavbarTop from './js/components/navbar-top/NavbarTop';
import MainContainer from './js/components/main-container/MainContainer';
import Loader from './js/components/Loader';
import $ from 'jquery'
import { connect } from 'react-redux'
import { stayLogged,listCand } from './js/actions/actions'
import Fade from '@material-ui/core/Fade';
import Login from './js/components/Login';
import CompleteCad from './js/components/CompleteCad';
import { bindActionCreators } from 'redux';


class App extends Component {
  
  constructor(props){
    super(props)
    this.state={
      canShow:false
    }
  }

  componentWillMount(){
    var token = localStorage.getItem('token');
    (token !== "undefined" || token !== null)
        ? (this.props.stayLogged(token,this.props.listCand),
          setTimeout(()=>{
            this.setState({canShow:true})
          },1000)
          )
        : setTimeout(()=>{
          this.setState({canShow:true})
        },1000)
  }
  componentDidMount() {
    
    var object1 = {};
    Object.defineProperties(object1, {
      property1: {
        value: {
          question: 'answer for life universe and everything',
          answer: 42
        },
        writable: true
      },
      property2: {}
    });

    // expected output: 42
  }

  render() {
    var token = localStorage.getItem('token')
    return (
      this.state.canShow
        ? (!this.props.logged
          ? <Login cadastro={this.props.cadastro} request={this.props.request} />
          : this.props.hasAttr
            ? <div className="App">
              <NavbarTop />
              <MainContainer />
            </div>
            : this.props.request
              ? <Loader />
              : <CompleteCad />)
            : null)
      ;
  }
}

const mapStateToProps = (state) => {
  return {
    cadastro: state.cadastro,
    request: state.request,
    logged: state.logged,
    isLoaded: state.isLoaded,
    hasAttr: state.hasAttr
  }
}
const mapDispatchToProps = (dispatch)=>{
return{
  stayLogged:bindActionCreators(stayLogged,dispatch),
  listCand:bindActionCreators(listCand,dispatch)
}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
