import initialState from './initialState.js'
import * as types from './actions/actionTypes.js'
import {filtro} from './actions/actionTypes'

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_MAIN:
            return Object.assign({}, state, {
                isVagaShow: (state.showVaga && action.page != 1) ? false : false,
                vagaOn: (state.showVaga && action.page != 1) ? "" : "",
                mainPage: action.page,
                newVaga:false,
                editVaga:false,
                showCand: false,
                filter:filtro.SHOW_ALL,
                category:[],
                editVaga:false,
                vagaEditar:[]
            })
            break;
        case types.LOADED:
            return Object.assign({}, state, {
                isLoaded: true,
                
            })
            break;
        case types.TOGGLE_FAV:
            return Object.assign({}, state,{
                candidatos: state.candidatos.map((cand) =>
                (cand._id === action.id)
                ? {...cand, isFav:!cand.isFav}
                : cand
            )
            })
            break
        case types.SHOW_VAGA:
       
            
            return Object.assign({},state,{
                showCand: false,
                isVagaShow: true,
                vagaOn:action.id,
                filter:filtro.SHOW_ALL,
                category:[]
            })
        case types.SHOW_CAND:
       
            
            return Object.assign({},state,{
                showCand: true,
                request:false,
                candidato:action.payload
            })
        case types.LIST_CAND:
       
            
            return Object.assign({},state,{
                listCand:action.payload
            })
        case types.VAGA_CRIADA:
   
            return Object.assign({},state,{
                criandoNovaVaga: false
            })
        case types.CRIANDO_VAGA:
      
            
            return Object.assign({},state,{
                criandoNovaVaga: true
            })
        case types.GET_INFO_VAGAS:
            
            return Object.assign({},state,{
                isVagaShow: true,
                candidatosVagaOn:action.payload
            })

        case types.SET_VISIBILITY_FILTER:
            
            switch(action.filter){
                case filtro.SHOW_ALL:
                return Object.assign({},state,{
                    filter:filtro.SHOW_ALL,
                    category:[]
                })

                default:
                return Object.assign({},state,{
                    filter:filtro.SHOW_FILTERED,
                    category: state.category.some((n)=> n.filtro == action.filter.filtro) ? [...state.category] : [...state.category,action.filter]
                })

            }

        case types.NEW_VAGA:
            return Object.assign({},state,{
                newVaga:true,
                vagaOn:true,
                
            })
        case types.LOGGED:
            action.payload.token
            ? localStorage.setItem('token',action.payload.token)
            : null
            return Object.assign({},state,{
                logged:true,
                hasAttr:true,
                request:false,
                user: {
                    rua:action.payload.empresa.rua,
                    telefone:action.payload.empresa.telefone,
                    id:action.payload.empresa.id,
                    bairro:action.payload.empresa.bairro,
                    complemento:action.payload.empresa.complemento,
                    numero:action.payload.empresa.complemento,
                    company: action.payload.empresa.nome,
                    CNPJ:action.payload.empresa.CNPJ,
                    telefone:action.payload.empresa.telefone,
                    cep:action.payload.empresa.cep,
                    estado:action.payload.empresa.estado,
                    cidade:action.payload.empresa.cidade,
                    email: action.payload.empresa.email,
                    token:action.payload.token ? action.payload.token : localStorage.getItem('token'),
                    isActive: true,
                    img: "https://via.placeholder.com/200x200"
                  }
            })
            
        case types.COMPLETE_CAD:
            return Object.assign({},state,{
                logged:true,
                hasAttr:true,
                request:false,
                user: {
                    company: action.info.nome,
                    CNPJ:action.info.empresa.CNPJ,
                    telefone:action.info.telefone,
                    cep:action.info.cep,
                    estado:action.info.estado,
                    cidade:action.info.cidade,
                    email: action.info.email,
                    isActive: true,
                    img: "https://via.placeholder.com/200x200"
                  }
            })
        case types.LOGGED_BUT:
            return Object.assign({},state,{
                logged:true,
                request:false,
                hasAttr:false,
                user: {
                    email: action.payload.empresa.email,
                    token:action.payload.token,
                    isActive: true,
                    img: "https://via.placeholder.com/200x200"
                  }
            })
        case types.EDIT_VAGA:
            return Object.assign({},state,{
                editVaga:true,
                vagaEditar:state.vagas.filter((n)=>n.id === action.id)[0]
            })
        case types.CADASTRO_FORM:
            return Object.assign({},state,{
                cadastro:true
            })
        case types.LOGIN_FORM:
            return Object.assign({},state,{
                cadastro:false
            })
        case types.CADASTRAR:
            return Object.assign({},state,{
                cadastro:false,
                logged:false,
                request:false
            })
        case types.REQUESTING:
            return Object.assign({},state,{
                request:true
            })
        case types.GET_PAYLOAD:
            return Object.assign({},state,{
                request:false
            })
        case types.GET_VAGAS:
            return Object.assign({},state,{
                request:false,
                vagas:action.payload.message ? [] : action.payload
            })
        case types.UPDATE_VAGAS:
            return Object.assign({},state,{
                vagas:action.payload.message ? [] : action.payload,
                isVagaShow: false,
                vagaOn:"",
                mainPage: 1,
                newVaga:false,
                editVaga:false,
                vagaEditar:[]
            })
        case types.DELETE_FILTER:
        console.log(state.category.length === 1);
        
        return !(state.category.length === 1)
            ? Object.assign({},state,{
                category: state.category.filter((n)=> n.filtro != action.category)
            })
            : Object.assign({},state,{
                filter:filtro.SHOW_ALL,
                category:[]
            })
        case types.LOGOUT:
            localStorage.setItem('token',null)
            return Object.assign({},state,initialState)
        default:
            return state;
    }
}


export default reducer
