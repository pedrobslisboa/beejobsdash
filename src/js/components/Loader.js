import React, { Component } from 'react'
import Fade from '@material-ui/core/Fade'
import { connect } from 'react-redux'


 class Loader extends Component {
  render() {
    return (
      <Fade in={true}>
      <div className="loader">
        <svg className='logo-loader' version="1.1" id="Camada_1" x="0px" y="0px"
          viewBox="0 0 100 100" style={{ enableBackground: ' 0 0 100 100' }}>
          <polygon id="XMLID_4_" className="st0" style={{ fill: 'none', stroke: '#000000', strokeWidth: 2, strokeMiterlimit: 10 }} points="97.8,84.5 73.9,98.3 50,84.5 50,56.9 73.9,43.1 97.8,56.9 " />
          <polygon id="XMLID_3_" className="st0" style={{ fill: 'none', stroke: '#000000', strokeWidth: 2, strokeMiterlimit: 10 }} points="50,84.5 26.1,98.3 2.2,84.5 2.2,56.9 26.1,43.1 50,56.9 " />
          <polygon id="XMLID_2_" className="st1" style={{ fill: '#F29217' }} points="49.5,42.8 24.7,57.1 0,42.8 0,14.3 24.7,0 49.5,14.3 " />
          <polygon id="XMLID_1_" className="st0" style={{ fill: 'none', stroke: '#000000', strokeWidth: 2, strokeMiterlimit: 10 }} points="73.9,43.1 50,56.9 26.1,43.1 26.1,15.5 50,1.7 73.9,15.5 " />
        </svg>
        </div>
      </Fade>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoaded: state.isLoaded
  }
}

export default connect(mapStateToProps,null)(Loader)
