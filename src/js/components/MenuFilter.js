import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux'

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import Filter from './Filter';
import SubFilter from './SubFilter';


//Aqui temos de fato o menu que é renderizado com as informaçoes dos usuarios, mostrando bairros, cidades,formacoes

const ITEM_HEIGHT = 48;

class LongMenu extends React.Component {
  options = [
    {
      isSub:true,
      value:'Bairro',
      filters:this.props.candidatosVagaOn.map((n)=>n.bairro).filter((item, pos,array)=> array.indexOf(item) == pos)
      // ['Vila São João','Colégio','Copacabana']
    },
    {
      isSub:true,
      value:'Cidade',
      filters:this.props.candidatosVagaOn.map((n)=>n.cidade).filter((item, pos,array)=> array.indexOf(item) == pos)
    },
    {
      isSub:true,
      value:'Estado',
      filters:this.props.candidatosVagaOn.map((n)=>n.estado).filter((item, pos,array)=> array.indexOf(item) == pos)
    },
    {
      isSub:true,
      value:'Escolaridade',
      filters:this.props.candidatosVagaOn.map((n)=>n.escolaridade).filter((item, pos,array)=> array.indexOf(item) == pos)
    },
  ];

  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  
  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    console.log(this.options);
    
    const options = this.options;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div>
        <Button
          aria-label="More"
          aria-owns={open ? 'long-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <span>Filtros</span>
          <KeyboardArrowDown/>
        </Button>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200
            },
          }}
        >
        <MenuItem onClick={this.handleClose}>
            <KeyboardArrowLeft/>
          </MenuItem>
          <Filter filter='SHOW_ALL' onClick={this.handleClose}>
            Mostrar todos
          </Filter>
          <Filter filter={{tipo:'Favoritos',filter:'Favoritos'}}>
            Favoritos
          </Filter>
          {options.map((option,i) => (
            <SubFilter key={i} filters={option.filters} name={option.value}/> 
    
          ))}
        </Menu>
      </div>
    );
  }
}

const mapStateToProps = (state)=>{
return{
  candidatosVagaOn:state.candidatosVagaOn
}
}

export default connect(mapStateToProps,null)(LongMenu)
