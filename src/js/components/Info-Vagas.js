import React, { Component } from 'react'
import { connect } from 'react-redux'
import { filtro } from '../actions/actionTypes'
import Fade from '@material-ui/core/Fade';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Chip from '@material-ui/core/Chip';
import MenuFilter from './MenuFilter';
import Title from './main-container/main/Title';
import Container from './Container';
import Filter from './Filter';
import { deleteFilter } from '../actions/actions';
import CardCandidato from './Cards/CardCandidato';
import FlipMove from 'react-flip-move';
import Chart from './main-container/main/dashboard/Chart';

console.log();

function removeAcento(text) {
    text = text.toLowerCase();
    text = text.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a');
    text = text.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e');
    text = text.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i');
    text = text.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o');
    text = text.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u');
    text = text.replace(new RegExp('[Ç]', 'gi'), 'c');
    return text;
}

const filterCandidatos = (candidatos, filter, category) => {
    switch (filter) {
        case filtro.SHOW_ALL:
            return candidatos
        case filtro.SHOW_FILTERED:
            console.log(candidatos)
            return [...candidatos].filter(t => {
                return (category.some((n, i) => (n.tipo == 'Bairro')
                    ? (removeAcento(t.bairro) == removeAcento(n.filtro))
                    : (n.tipo == 'Cidade' && t.hasOwnProperty('cidade'))
                        ? (removeAcento(t.cidade) == removeAcento(n.filtro))
                        : (n.tipo == 'Escolaridade' && t.hasOwnProperty('escolaridade'))
                            ? (removeAcento(t.estado) == removeAcento(n.filtro))
                            : (n.tipo == 'Estado' && t.hasOwnProperty('estado'))
            ))
            })

        default:
            throw new Error('Unknown filter: ' + filter)
    }
}


class InfoVagas extends Component {


    render() {

        console.log(this.props.category);


        return (
            <Fade in>
                <div className="info-vagas">
                    <Container>
                        <Title name='Assistente'>
                            <div className="chart-vaga">
                                <Chart percente={20} />
                            </div>
                        </Title>

                    </Container>
                    <div className="filter-chip">
                        <MenuFilter />
                        {this.props.category.map((n, index) =>
                            <Chip
                                className="chip"
                                key={index}
                                label={`${n.tipo}: ${n.filtro}`}
                                color="primary"
                                onDelete={() => this.props.deleteFilter(n.filtro)}
                            />
                        )}
                    </div>
                    <FlipMove className="cards-body">
                        {this.props.candidatosVagaOn.map((n, i) => <CardCandidato idCand={n.id} dist={n.distancia} key={n._id} n={i}>
                            {n.nome}
                        </CardCandidato>)}
                    </FlipMove>
                </div>
            </Fade>



        )
    }

}

const mapStateToProps = (state) => {
    console.log(state.filter);
    return {
        category: state.category,
        candidatosVagaOn: filterCandidatos(state.candidatosVagaOn, state.filter, state.category),
        vagaOn: state.vagaOn
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        deleteFilter: function (category) {
            dispatch(deleteFilter(category))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InfoVagas)
