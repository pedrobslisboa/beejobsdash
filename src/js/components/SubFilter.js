import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Filter from './Filter';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';

const ITEM_HEIGHT = 48;

class LongMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div>
        <MenuItem
        style={{display:'flex', justifyContent:'space-between'}}
          aria-label="More"
          aria-owns={open ? 'long-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <span>{this.props.name}</span>
          <KeyboardArrowDown/>
        </MenuItem>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200,
            },
          }}
        >
          <MenuItem onClick={this.handleClose}>
            <KeyboardArrowLeft/>
          </MenuItem>
          {this.props.filters.map((filter,i) => (
            <div onClick={this.handleClose}>
            <Filter key={i} selected={filter === 'Pyxis'} filter={{tipo: this.props.name, filtro: filter }}>
            {filter}
            </Filter> 
            </div>
                  ))}
        </Menu>
      </div>
    );
  }
}


export default LongMenu;