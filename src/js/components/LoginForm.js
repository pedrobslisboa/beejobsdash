import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import logo from '../../imgs/logo.jpg'
import { connect } from 'react-redux'
import { logged, loggedBut } from '../actions/actions'
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import green from '@material-ui/core/colors/green';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade'

//Form de login básico

export default class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            usuario: '',
            senha: '',
            notFill: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.logar = this.logar.bind(this)
    }

    // Altera o state de acordo com o nome
    handleChange = name => e => {
        console.log(name);
        console.log(e);
        
        this.setState({
            [name]: e.target.value
        })
    }


    // fn de logar
    logar() {
        console.log(this.state.usuario,this.state.senha);
        
        (this.state.usuario != '' && this.state.senha != '')
            ? this.props.logged({
                email: this.state.usuario,
                senha: this.state.senha
            },this.props.listCand)
            : this.setState({ notFill: true })

    }
    render() {
        return (
            <div>
                <Fade in={!this.props.request}>
                    <div className="login-form">
                        <img src={logo} style={{ width: '150px' }} />
                        { this.state.notFill
                            ?<div>Por favor, preencha todos os dados</div>
                        :null
                            }
                        <form>
                            <TextField label="Email" onChange={this.handleChange('usuario')} value={this.props.usuario} />
                            <TextField label="Senha" onChange={this.handleChange('senha')} type="password" value={this.props.senha} />
                        </form>
                        <MuiThemeProvider theme={
                            createMuiTheme({
                                palette: {
                                    primary: green
                                }
                            })}>
                            <Button color="primary" variant="contained"
                                onClick={this.logar}
                                className='button'
                            >Login</Button>
                            <div onClick={this.props.cadastroForm}>
                                cadastrar-se
        </div>
                        </MuiThemeProvider>


                        {/*<Button onClick={this.props.loggedBut} className='button'>Test</Button>*/}
                    </div>
                </Fade>
            </div>
        )
    }
}
