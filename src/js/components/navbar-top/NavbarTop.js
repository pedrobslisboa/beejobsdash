import React, { Component } from 'react'
import logo from '../../../logo.svg'
import Avatar from '@material-ui/core/Avatar'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import Menu from './Menu'

// NavbarTop é a parte de cima do Dash, contendo o menu, o greating do usuário e o menu, além do logo
export default class NavbarTop extends Component {

    constructor(props){
        super(props)
        this.state ={
         open:false   
        }
        this.handleToggle = this.handleToggle.bind(this)
    }

    // Troca entre open e not open da janela
    handleToggle(){
        this.setState({
            open:!this.state.open
        })
    }
    
    render() {
        return (
            <nav className='navbar-top'>
                <nav>
                    <img className='logo' src={logo}></img>
                    <div className='hello'>
                        <div className="info">
                        <span>Olá {/* Aqui será colocado o nome do usúario após resquisiçao*/}</span>
                        <Avatar>H</Avatar>
                        <ArrowDropDown
         
            aria-owns={this.state.open ? 'menu-list-grow' : null}
            aria-haspopup="true"
            onClick={this.handleToggle.bind(this)}
          />
          </div>
                    <Menu className="dropdown" handleToggle={this.handleToggle} open={this.state.open}/>
                    </div>
                </nav>
            </nav>
        )
    }
}
