

import React, { Component } from 'react'
import { connect } from 'react-redux'

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import {logout} from '../../actions/actions';
import { bindActionCreators } from 'redux';

// Esse é o menu que fica no NavbarTop

class Menu extends Component {

  render() {
    return (
        <div >
        <Popper className= {this.props.className} open={this.props.open} anchorEl={this.anchorEl} transition disablePortal>
        {({ TransitionProps }) => (
          <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{ transformOrigin:'center top' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={this.props.handleToggle}>
                <MenuList>
                  <MenuItem onClick={this.props.logout}>Logout</MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch)=>{
return{
  logout:bindActionCreators(logout,dispatch)
}
}

export default connect(null,mapDispatchToProps)(Menu)