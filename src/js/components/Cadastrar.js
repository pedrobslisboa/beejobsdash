import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import logo from '../../imgs/logo.jpg'
import { connect } from 'react-redux'
import { logged, loggedBut } from '../actions/actions'
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import green from '@material-ui/core/colors/green';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade'

export default class Cadastrar extends Component {
    constructor(props){
        super(props)
        this.state = {
           usuario:'',
           senha:'',
           confSenha:''
        }
        this.allFill = this.allFill.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    // Verifica se tudo foi preenchido
    allFill(){
        return (this.state.usuario !== '' && this.state.senha !== '' && this.state.confSenha !== '')
        ? true
        : false
    }


    // Muda o state, baseado no nome
    handleChange = (nome) => (e) => {
        
        this.setState({
            [nome]:e.target.value
        })
    }

    // Efetivamente vai cadastrar
    cadastrar(){
        this.allFill
        ? this.props.cadastrar({
            email:this.state.usuario,
            senha:this.state.senha
        })
        : null
    }

  render() {
    return (
        <div className="">
        <Fade in={!this.props.request}>
        <div className="login-form">
        <img src={logo} style={{ width: '150px' }} />
        <form>
        <TextField onChange={this.handleChange("usuario")} label="Usuário" />
        <TextField onChange={this.handleChange("senha")} label="Senha" />
        <TextField onChange={this.handleChange("confSenha")} label="Confirmar Senha" />
        </form>
        <MuiThemeProvider theme={
          createMuiTheme({
            palette: {
              primary: green
            }
          })}>
          <Button color="primary" variant="contained"
            onClick={this.cadastrar.bind(this)}
            className='button'
          >Cadastrar</Button>
          <div onClick={this.props.loginForm}>
        Login
        </div>
        </MuiThemeProvider>
  
        
        {/*<Button onClick={this.props.loggedBut} className='button'>Test</Button>*/}
        </div>
        </Fade>
        </div>
    )
  }
}
