import React, { Component } from 'react'
import Zoom from '@material-ui/core/Zoom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import AddIcon from '@material-ui/icons/Add';

// Esta é o card de nova vaga, que é transparente e fica apos os cards de vagas

export default class NewVagasCard extends Component {
  render() {
    return (
        <Zoom in={true} style={{ transitionDelay: this.props.n*50 }}>
        <Card onClick={this.props.onClick} className='new-card' style={{display:'flex',alignItems:'center',justifyContent:'center',width:'250px', height:'150px', backgroundColor:'transparent', border:'1px solid grey'}}>

        <AddIcon style={{ float:'left', border: '1px solid black',width:'50px',height:'50px',borderRadius:'50%'}}/>

      </Card>
      </Zoom>
    )
  }
}
