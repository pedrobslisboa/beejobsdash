import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import logo from '../../imgs/logo.jpg'
import { connect } from 'react-redux'
import { logged, loggedBut,cadastroForm,loginForm,cadastrar,completeCad,listCand } from '../actions/actions'
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import green from '@material-ui/core/colors/green';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade'
import Loader from './Loader';
import LoginForm from './LoginForm';
import Cadastrar from './Cadastrar';



class Login extends Component {

  render() {
    return (

      <div className="login">
      <Paper className='paper' elevation={1}>
          {!this.props.cadastro
          ?<LoginForm cadastroForm={this.props.cadastroForm} logged={this.props.logged} listCand={this.props.listCand} request={this.props.request}/>
          :<Cadastrar completeCad={(info)=>this.props.completeCad(info)} cadastrar={this.props.cadastrar} loginForm={this.props.loginForm} logged={this.props.logged} request={this.props.request}/>}
         {this.props.request
          ?<Loader/>
          : null
         }
        </Paper>
      
      </div >
      
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    listCand:bindActionCreators(listCand,dispatch),
    completeCad:bindActionCreators(completeCad,dispatch),
    cadastrar:bindActionCreators(cadastrar,dispatch),
    cadastroForm:bindActionCreators(cadastroForm,dispatch),
    loginForm:bindActionCreators(loginForm,dispatch),
    logged: bindActionCreators(logged, dispatch),
    loggedBut: bindActionCreators(loggedBut, dispatch),
  }
}

export default connect(null, mapDispatchToProps)(Login)
