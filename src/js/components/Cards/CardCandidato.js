import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import Zoom from '@material-ui/core/Zoom';
import Avatar from '@material-ui/core/Avatar';
import Bookmark from '@material-ui/icons/BookmarkTwoTone';
import FavSvg from '../main-container/main/dashboard/FavSvg';
import { bindActionCreators } from 'redux';
import {showCand} from '../../actions/actions'
import { connect } from 'react-redux'

// Card do candidato, que contem a foto e nome do candidato alem do onClick que mostra o mesmo

class CardCandidato extends Component {
  render() {
    return (
      <Zoom in={true} style={{transitionDelay: this.props.n*50 }}>
      <div className='card' style={{minWidth:'25%',height:'200px'}}>
      <Card onClick={()=>this.props.showCand(this.props.idCand,this.props.dist)} className='card-candidatos'>
        <Bookmark className="fav-icon"/>
        <Avatar style={{minWidth:'100px',height:'100px'}}>H</Avatar>
        {this.props.children}
      </Card>
      </div>
      </Zoom>
    )
  }
}


const mapDispatchToProps = (dispatch)=>{
return{
  showCand: bindActionCreators(showCand,dispatch)
}
}

export default connect(null,mapDispatchToProps)(CardCandidato)
