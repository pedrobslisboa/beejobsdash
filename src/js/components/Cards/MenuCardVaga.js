import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';

// Neste componente temos o menu que está nos cards de vaga, e contem o delete e edit

const options = [
  'editar',
'deletar'

];

const ITEM_HEIGHT = 48;

class LongMenu extends React.Component {
    constructor(props){
        super(props)
    }
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  finalizar = () => {
    this.setState({ anchorEl: null });
    this.props.finalizarVaga()
  };
  editar = () => {
    this.setState({ anchorEl: null });
    this.props.editVaga()
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div style={this.props.style}>
        <IconButton
          aria-label="More"
          aria-owns={open ? 'long-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5
            },
          }}
        >
            <MenuItem  onClick={this.editar}>
              Editar
            </MenuItem>
            <MenuItem  onClick={this.finalizar}>
              Finalizar
            </MenuItem>
        </Menu>
      </div>
    );
  }
}

export default LongMenu;