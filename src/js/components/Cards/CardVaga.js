import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Title from '../main-container/main/Title';
import Zoom from '@material-ui/core/Zoom';
import Button from '@material-ui/core/Button';
import MenuCardVaga from './MenuCardVaga';

// Card que contem a vaga, mostrando a quantidade de candidatos, nome da vaga e favoritos


class CardVaga extends Component {
  constructor(props){
    super(props)
    this.state ={
      remove:false
    }
  }

  remover(){
    this.setState(
      {remove:true}
    )
  }

  render() {
    return (
    <Zoom in={!this.state.remove} style={{ transitionDelay: this.props.n*50 }}>
        <Card className='card'>
        <CardContent className='content'>
        <div className="cor-vaga">
        </div>
        <div className="info">
        <MenuCardVaga remover={this.remover.bind(this)} editVaga={this.props.editVaga} finalizarVaga={this.props.finalizarVaga} style={{position:'absolute',top:0,right:0}}/>
        <Title name={this.props.nome}/>
          <div className="candidatura">
          <div className="candidatos"><span className="numero-candidatos">{this.props.candidaturas}</span>
          <span>Candidatos</span></div>
          <div className="favs"><span className="numero-fav">{this.props.favoritos}</span><span>Favoritos</span></div>
        </div>
        </div>

        </CardContent>
        <CardActions style={{backgroundColor:'#DEDFDF'}}>
        <Button onClick={this.props.onClick} className='button' variant="outlined" >
            Ver vaga
        </Button>
        </CardActions>
      </Card>
      </Zoom>
    )
  }
}

export default CardVaga