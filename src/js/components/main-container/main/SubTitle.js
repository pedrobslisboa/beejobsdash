import React, { Component } from 'react'

// Subtitulo usado no perfil do candidato, porém é um componente reutilizavel

export default class SubTitle extends Component {
  render() {
    return (
        <div className='subtitle'><h1>{this.props.name}</h1>{this.props.children}</div>
    )
  }
}
