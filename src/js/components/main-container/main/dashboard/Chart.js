import React, { Component } from 'react'
import { connect } from 'react-redux'

import $ from 'jquery';

// Esse aqui é o meu amorzinho, o Chart foi criado para ter uma animaçao 

class Chart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      circunference: 0,
      percente: 0,
    }
    this.calculatePercent = this.calculatePercent.bind(this)
    this.load = this.load.bind(this)
    this.resize = this.resize.bind(this)
  }


  calculatePercent(d) {

    this.circunference = ((d) * (40 / 100)) * 3.14 * 2;
    this.percente = (this.circunference / 100) * this.props.percente;
    this.setState({
      circunference: this.circunference,
      percente: this.percente
    })
  }

  load() {

    this.calculatePercent($('.circle-chart').width());

    this.percente = (this.circunference / 100) * this.props.percente;
    let styleSheet = document.styleSheets[0];

    let animationName = `animation${Math.round(Math.random() * 100)}`;

    let keyframes =
      `@-webkit-keyframes ${animationName} {
          to {stroke-dasharray: 0 ${this.circunference}}
      }`;


    styleSheet.insertRule(keyframes, styleSheet.cssRules.length);

    this.setState({
      animationName: animationName
    });

  }

  resize() {
    $('.circle-chart__circle').css({
      strokeDasharray: `${this.state.percente},${this.state.circunference}`
    })
  }

  componentDidMount() {
    this.load();
    window.addEventListener('resize', () => {
      this.calculatePercent($('.circle-chart').width());
      console.log(this.state.percente);

      console.log($('.circle-chart').width());

    });
  }

  render() {
    let style = {
      animationName: this.state.animationName,
      animationTimingFunction: 'easeInOutExpo',
      animationDuration: '1.5s',
      animationDelay: '0.0s',
      animationIterationCount: 1,
      animationDirection: 'reverse',
      animationFillMode: 'forwards'
    };
    

    return (
        <div className="chart-box">
          <div className="chart">
            <svg className="circle-chart" viewBox="0 0 90% 90%" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
              <circle className="circle-chart__background" stroke="#4D4C4C" strokeWidth="17%" fill="none" cx="50%" cy="50%" r="40%" />
              <circle className="circle-chart__circle" style={this.props.logged ? style : {}} stroke="#F79428" strokeWidth="19%" strokeDasharray={`${this.percente},${this.state.circunference}`} fill="none" cx="50%" cy="50%" r="40%" />
            </svg>
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state)=>{
    return{
      logged:state.logged
    }
}

export default connect(mapStateToProps,null)(Chart)