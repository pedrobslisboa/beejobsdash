import React, { Component } from 'react'
import { connect } from 'react-redux'

import Chart from './Chart.js'
import Title from '../Title'

class ChartLeft extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){

    console.log(this.props.nCandidatos*100/this.props.vagas.reduce((a, b) => ({nVagas: a.nVagas + b.nVagas})).nVagas)
    
  }

  render() {
    return (
      <div className="container dash-left">
      <div className="infos">
      <Title name={'Critérios das vagas'}/>
          <ul>
            <li>Não obedecem aos critérios da vaga</li>
            <li>Obedecem aos critérios da vaga</li>
          </ul>
          <span className='filter'>ultimos 30 dias</span>
        </div>
        <Chart percente={this.props.nCandidatos*100/this.props.vagas.reduce((a, b) => ({visualizacoes: a.visualizacoes + b.visualizacoes})).visualizacoes >= 100 ? 100 : this.props.nCandidatos*100/this.props.vagas.reduce((a, b) => ({visualizacoes: a.visualizacoes + b.visualizacoes})).visualizacoes}/>
      </div>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    nCandidatos: state.candidatos.length,
    vagas: state.vagas
  }
}

export default connect (mapStateToProps,null)(ChartLeft);