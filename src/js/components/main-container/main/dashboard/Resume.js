import React, { Component } from 'react'
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/ExpandMore';
import CardVaga from '../../../Cards/CardResume.js'
import Animate from 'react-move/Animate'
import CardResume from '../../../Cards/CardResume'

// Resume contem o que poderia ser um resumo no Dash principal, nao implementado mas com codigo aqui para
// possivel implementacao

class Resume extends Component {

    state = {
        checked: false,
    };

    onClick(){
        this.setState({checked:!this.state.checked})
    }

    render() {

        const { checked } = this.state;
        return (

            <div style={{zIndex:0}} >
            <div className='container resume' style={{minHeight:'40px', marginBottom:'-5px',zIndex:2}}>
            <Animate
          start={() => ({
            x: 0,
          })}

          update={() => ({
            x: [checked ? 180 : 0],
            timing: { duration: 50},
          })}
        >
          {(state) => {
            const { x } = state;

            return (
              <AddIcon className='expand' onClick={this.onClick.bind(this)}
                  style={{cursor:'pointer', transform: checked ? 'rotate(45deg)' : '', WebkitTransform: `rotate(${x}deg)`,transform: `rotate(${x}deg)`}}
                />
            );
          }}
        </Animate>
                
            </div>
                <Collapse in={checked}>
                    <div className='container' style={{backgroundColor:'#DEDFDF',marginTop:'-5px', height:'200px',zIndex:1}}>
                        <CardResume n={0}/>
                        <CardResume n={1}/>
                        <CardResume n={2}/>
                    </div>
                </Collapse>
            </div>

        )
    }
}


export default Resume