import React, { Component } from 'react'
import DashLeft from './DashLeft.js'
import DashRight from './DashRight.js'

export default class DashBottom extends Component {
    render() {
        return (
            <div className='dash-bottom'>
               {/* <DashLeft />*/}
                <DashRight />
            </div>
        )
    }
}
