import React, { Component } from 'react'
import Tabela from '../Tabela.js'
import Title from '../Title.js'



export default class DashTop extends Component {

  constructor(props){
    super(props)
    
  }

  componentDidMount(){
    console.log(this.props.candidatos);
  }


  render() {
    return (
      <div className='container dash-top'>
       <Title name='Dashboard' complement='Bla'/>
       <Tabela listCand={this.props.listCand} showFavIcon={true} table={this.props.candidatos}/> 
      </div>
    )
  }
}
