import React, { Component } from 'react'
import Chart from './Chart.js'
import Title from '../Title.js'
export default class DashRight extends Component {
  render() {
    return (
      <div className='container dash-right'>
      <div className="infos">
      <Title name={'Visualizações de vagas'}/>
          <ul>
            <li>Sem candidaturas</li>
            <li>Candidaturas</li>
          </ul>
          <span className='filter'>ultimos 30 dias</span>
        </div>
      <Chart percente={20} />
      </div>
    )
  }
}
