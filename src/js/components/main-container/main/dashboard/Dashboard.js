import React, { Component } from 'react'
import { connect } from 'react-redux'

import DashTop from './DashTop'
import DashBottom from './DashBottom'
import Resume from './Resume';
import Fade from '@material-ui/core/Fade';

// Aqui temos a parte mais complicada, o dash sofreu VARIAS atualizaçoes no decorrer do tempo
// Por isso temos o DashTop ai, caso olhe para a pasta dashboard verá altos arquivos com relaçao ao dash
// DashBottom por exemplo era encarregado de conter os Charts, o Bottom continha DashLeft e Dash Right com 
// um Chart cada um.

 class Dashboard extends Component {

  render() {
    return (
      <Fade in={true}>
      <div className="dashboard">
        <DashTop listCand={this.props.listCand} candidatos={this.props.candidatos}/>
      </div>
      </Fade>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    candidatos: state.candidatos
  }
}



export default connect(mapStateToProps,null)(Dashboard) 
