import React, { Component } from 'react'
import { connect } from 'react-redux'

import Tabela from '../Tabela';
import Dashboard from '../dashboard/Dashboard';
import Fade from '@material-ui/core/Fade';

import NewCardVaga from '../../../../components/NewVagasCard'
import CardVaga from '../../../Cards/CardVaga';
import { getInfoVaga } from '../../../../actions/actions'
import { createVaga,finalizarVaga,upInfoCompany,editVaga } from '../../../../actions/actions'
import InfoVagas from '../../../Info-Vagas'
import NewVaga from './NewVaga';
import EditVaga from './EditVaga';
import { bindActionCreators } from 'redux';
import FlipMove from 'react-flip-move';
import PerfilCandidato from '../candidato/PerfilCandidato';

// Aqui é organizado tando as vagas quanto a tabela
// NewVaga é o form para criar nova vaga que abre ao ter o state newVaga como true
// isVagaShow mostra o InfoVagas que é a info da vaga clicada, ao clicar no usuario 
// o showCand vira true mostrando PerfilCandidato
// EditVaga vem ao clicar no editar, editVaga vira true

class Vagas extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    console.log(this.props.criandoNovaVaga);
    
    return (
      <Fade in={true}>
        {this.props.newVaga
            ? <NewVaga criandoNovaVaga={this.props.criandoNovaVaga}/>
            : <div>
              <div className='vagas'>
              {this.props.isVagaShow
              ? this.props.showCand
                ? <PerfilCandidato candidato={this.props.candidato}/>
                :<InfoVagas />
              : this.props.editVaga
              ? <EditVaga/>
              : <div className='vagas-cards'>
              <Dashboard listCand={this.props.listCand} /> 
                {this.props.vagas.map((n, i) => {
                  return <CardVaga candidatosVagaOn={this.props.candidatosVagaOn} editVaga={()=>this.props.editar(n.id)} idVaga={n.id} onClick={() => this.props.getInfoVaga(n.id)} finalizarVaga={()=>{
                    this.props.finalizarVaga(n.id,()=>{
                      this.props.upInfoCompany()
                    })
                  }} candidaturas={n.candidaturas} favoritos={n.favoritos} nome={n.cargo} n={i} />
                }
                )}
                <NewCardVaga onClick={this.props.createVaga} n={this.props.vagas.length} />
                
  
                </div>}
              </div>
            </div>}
      </Fade>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    candidato:state.candidato,
    listCand:state.listCand,
    showCand:state.showCand,
    editVaga:state.editVaga,
    criandoNovaVaga:state.criandoNovaVaga,
    candidatosVagaOn:state.candidatosVagaOn,
    vagas: state.vagas,
    isVagaShow: state.isVagaShow,
    newVaga: state.newVaga
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    editar: bindActionCreators(editVaga,dispatch),
    finalizarVaga: bindActionCreators(finalizarVaga,dispatch),
    upInfoCompany: bindActionCreators(upInfoCompany,dispatch),
    getInfoVaga: function (id) {
      dispatch(getInfoVaga(id))
    },
    createVaga:bindActionCreators(createVaga,dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Vagas)