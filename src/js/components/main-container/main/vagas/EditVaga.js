import React, { Component } from 'react'
import { connect } from 'react-redux'

import Fade from '@material-ui/core/Fade'
import Container from '../../../Container';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Title from '../Title';
import CircularProgress from '@material-ui/core/CircularProgress'
import MenuItem from '@material-ui/core/MenuItem'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import $ from 'jquery'
import axios from 'axios'
import { bindActionCreators } from 'redux';
import { sendNewVagas, updatingVagas,concEdit } from '../../../../actions/actions'
import CircularIntegration from './ButtonNewVaga'
import AutoComplete from './AutoComplete'

// Form de ediçao de vagas

class NewVaga extends Component {
  state = {
    cargo: this.props.vagaEditar.cargo,
    descricao: this.props.vagaEditar.descricao,
    contrato: this.props.vagaEditar.tipoContrato,
    salario: this.props.vagaEditar.salario,
    cep: this.props.vagaEditar.cep,
    bairro: this.props.vagaEditar.bairro,
    rua: this.props.vagaEditar.rua,
    cidade: this.props.vagaEditar.cidade,
    numero: this.props.vagaEditar.numero,
    uf: this.props.vagaEditar.estado,
    complemento: this.props.vagaEditar.complemento
  }


  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  }

  sameEd() {
    this.setState({
      isSame: !this.state.isSame,
    });
  }

  onBlur(e) {
    var target = this;
    console.log(target);

    var currentTarget = e.currentTarget;
    var cep = $(e.target).val().replace(/\D/g, '');
    setTimeout(function () {
      if (!currentTarget.contains(document.activeElement)) {
        if (cep != "") {
          axios.get(`https://viacep.com.br/ws/${cep}/json`)
            .then(res => res.data)
            .then(data =>

              target.setState({
                rua: data.logradouro,
                bairro: data.bairro,
                uf: data.uf,
                cidade: data.localidade
              })

            )
        }
      }
    }, 0);
  }

  createNewVaga() {
    
    const infos = {
      "cargoId": 2,
      "cargo": this.state.cargo,
      "salario": this.state.salario,
      "tipoContrato": this.state.contrato,
      "PCD": false,
      "descricao": this.state.descricao,
      "cep": this.state.cep,
      "estado": this.state.uf,
      "cidade": this.state.cidade,
      "bairro": this.state.bairro,
      "rua": this.state.rua,
      "numero": this.state.numero,
      "complemento": this.state.complemento
    }
    console.log(infos);
    this.props.concEdit(infos,this.props.vagaEditar.id, this.props.updatingVagas)
  }

  render() {

    return (
      <Fade in >
        <div className='nova-vaga'>
       
        
          <Container>
            <Title name="Nova Vaga" />
            <form noValidate autoComplete="off">
              <div className="form">
                <div className="main-info">
                  <TextField

                    id="name"
                    label="Cargo"
                    onChange={this.handleChange('cargo')}
                    margin="normal"
                    className="input"
                    value={this.state.cargo}
                  />
                  <TextField
                    id="salario"
                    label="Salário"
                    style={{ width: '200px' }}
                    type="number"
                    margin="normal"
                    className="input"
                    onChange={this.handleChange('salario')}
                    value={this.state.salario}
                    select
                  >
                    <MenuItem value='A Combinar'>
                      A combinar
  </MenuItem>

                    <MenuItem value="Até 1.000">
                      R$ 1000-1500
  </MenuItem>
                    <MenuItem value='R$ 1.000 a 1.500'>
                      R$ 1.000 a 1.500
  </MenuItem>
                    <MenuItem value='R$ 1.500 a 2.000'>
                    R$ 1.500 a 2.000
  </MenuItem>
                    <MenuItem value='R$ 2.000 a 2.500'>
                    R$ 2.000 a 2.500
  </MenuItem>
                    <MenuItem value='R$ 2.500 a 3.000'>
                    R$ 2.500 a 3.000
  </MenuItem>
                    <MenuItem value='R$ 3.000 a 3.500'>
                    R$ 3.000 a 3.500
  </MenuItem>
                    <MenuItem value="R$ 3.500 a 4.000">
                    R$ 3.500 a 4.000
  </MenuItem>
                    <MenuItem value="R$ 4.000 a 4.500">
                    R$ 4.000 a 4.500
  </MenuItem>
                    <MenuItem value="R$ 4.500 a 5.000">
                    R$ 4.500 a 5.000
  </MenuItem>
                    <MenuItem value="Acima de 5.000">
                    Acima de 5.000
  </MenuItem>
                  </TextField>
                  <TextField
                    id="contrato"
                    label="Tipo de Contrato"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    select
                    margin="normal"
                    onChange={this.handleChange('contrato')}
                    value={this.state.contrato}
                  >
                    <MenuItem value='PJ'>
                      PJ
        </MenuItem>
                    <MenuItem value='CLT'>
                      CLT
        </MenuItem>
                    <MenuItem value='Estagio'>
                      Estágio
        </MenuItem>
                    <MenuItem value='Temp'>
                      Temporário
        </MenuItem>
                  </TextField>
             
                  <TextField
                    multiline
                    rows="4"
                    fullWidth
                    id="observacoes"
                    label="Descrição"
                    margin="normal"
                    className="input"
                    value={this.state.descricao}
                    onChange={this.handleChange('descricao')}
                  />

                </div>
                <div className="endereco">
                  <TextField
                    autoComplete='nope'
                    onBlur={this.onBlur.bind(this)}
                    onChange={this.handleChange('cep')}
                    disabled={this.state.isSame}
                    id="cep"
                    label="Cep"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.cep : this.state.cep}
                  />
                  <TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    id="rua"
                    label="Rua"
                    onChange={this.handleChange('rua')}
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.rua : this.state.rua}
                  /><TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    onChange={this.handleChange('bairro')}
                    id="bairro"
                    label="Bairro"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.bairro : this.state.bairro}
                  /> <TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    onChange={this.handleChange('numero')}
                    id="numero"
                    label="numero"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.numero : this.state.numero}
                  />
                  <TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    onChange={this.handleChange('cidade')}
                    id="cidade"
                    label="Cidade"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.cidade : this.state.cidade}
                  />
                  <TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    onChange={this.handleChange('uf')}
                    id="uf"
                    label="UF"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.uf}
                    value={this.state.isSame ? this.props.vagaEditar.estado : this.state.uf}
                  />
                  <TextField
                    autoComplete='nope'
                    disabled={this.state.isSame}
                    id="complemento"
                    label="Complemento"
                    style={{ width: '200px' }}
                    margin="normal"
                    className="input"
                    value={this.state.isSame ? this.props.vagaEditar.complemento : this.state.complemento}
                  />
                </div>
              </div>
            </form>
            <CircularIntegration name="Editar vaga" criandoNovaVaga={this.props.criandoNovaVaga} onClick={this.createNewVaga.bind(this)} />
          </Container>
        </div>
      </Fade>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    vagaEditar: state.vagaEditar
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    concEdit: bindActionCreators(concEdit, dispatch),
    updatingVagas: bindActionCreators(updatingVagas, dispatch),
    sendNewVagas: bindActionCreators(sendNewVagas, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewVaga)