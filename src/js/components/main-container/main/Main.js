import React, { Component } from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'

import Dashboard from './dashboard/Dashboard.js'
import Vagas from './vagas/Vagas.js'
import Loader from '../../Loader.js';
import {getInfoCompany} from '../../../actions/actions'

// Este é o main que contem a parte esquerda do nosso dash
// Loader é para indicar quando algo é carregado, o loader é sempre chamado quando o estado request
// for true
// Vagas é virou a parte principal após decisão da equipe em manter as vagas na primeira página

class Main extends Component {
  componentDidMount(){
    console.log(this.props.token);
    // REcebe o token
    this.props.getInfoCompany(this.props.token)
  }
  render() {
    return (
      <main className='main'>
        {this.props.request
          ? <Loader/>
          : <div> 
            <Vagas />
            </div>}
      </main>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    mainPage: state.mainPage,
    request: state.request,
    token: state.user.token
  }
}

const mapDispatchToProps = (dispatch)=>{
return{
  getInfoCompany:bindActionCreators(getInfoCompany,dispatch)
}
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
