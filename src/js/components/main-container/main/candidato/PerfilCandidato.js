import React, { Component } from 'react'
import Container from '../../../Container';
import Title from '../Title';
import SubTitle from '../SubTitle';

// Perfil do candidato

export default class PerfilCandidato extends Component {

  calculateTime(){
    
    
  }

  render() {
    console.log();
    return (
      <Container className="perfil">
        <Title name={this.props.candidato.nome}>
        <div className='photo'><img src="https://via.placeholder.com/200x200"></img></div>
        </Title>
        <div>
        <SubTitle name="Informações Gerais"/>
        <div className="info-gerais">
       
        </div>
        <SubTitle name="Formação"/>
        <div className="detalhes-formacao">
          {this.props.candidato.Formacoes.map((n,i)=> <div className="formacao" key={i}>{parseInt((new Date(n.dataTermino) - new Date(n.dataInicio))/(60*1000*60*24*30))} Meses<div className="info-prof">{n.tipoFormacao} em {n.nomeFormacao}</div><div className="info-prof">{(new Date(n.dataInicio)).getMonth()}/{(new Date(n.dataInicio)).getFullYear()} - {(new Date(n.dataTermino)).getMonth()}/{(new Date(n.dataTermino)).getFullYear()}</div></div>)}
        </div>
        <SubTitle name="Experiência Profissional"/>
        <div className="detalhes-exp">
        {this.props.candidato.ExperienciasProfissionais.map((n,i)=> <div className="exp" key={i}>
        <div className="duracao">{parseInt((new Date(n.dataSaida) - new Date(n.dataEntrada))/(60*1000*60*24*30))} Meses</div>
        <div>
        <div className="info-prof">{n.cargo} na empresa {n.empresa}</div><div className="info-prof">{(new Date(n.dataEntrada)).getMonth()}/{(new Date(n.dataEntrada)).getFullYear()} - {(new Date(n.dataSaida)).getMonth()}/{(new Date(n.dataSaida)).getFullYear()}</div></div></div>)}
        </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (dispatch)=>{
return{
    
}
}
