import React, { Component } from 'react'
import FavIcon from './dashboard/FavSvg.js'
import { connect } from 'react-redux'
import {toggleFav} from '../../../actions/actions'

// tabela contendo os 5 primeiros candidatos, a requisiçao é feita logo no inicio e pode ser verificada no actions

class Tabela extends Component {

  constructor(props){
    super(props)
  }


  render() {

    const tableToShow = this.props.listCand.map((n,i)=> <tr key={i}><td></td><td></td><td>{n.vaga.cargo}</td><td>{n.usuario.nome}</td><td>{n.usuario.escolaridade}</td><td><a>Visualizar</a></td><td></td></tr>) 

    return (
      <div className='table'>
      <table>
      <thead>
      <tr className='tr-head'>
        <th></th>
        <th></th>
        <th>Vagas</th>
        <th>Candidato</th>
        <th>Formação</th>
        <th>Currículo</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {tableToShow}
      </tbody>
      </table>
      </div>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onClick:function(id){
      dispatch(toggleFav(id));
    }
  }
}

export default connect(null,mapDispatchToProps)(Tabela)
