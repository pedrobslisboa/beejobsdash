import React, { Component } from 'react'

// componente title comumente usado nos containers

export default class Title extends Component {

constructor(props){
    super(props)
}

  render() {
    return (
        <div className='title'><h1>{this.props.name}</h1>{this.props.children}</div>
    )
  }
}
