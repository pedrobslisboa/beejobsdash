import React, { Component } from 'react'
import NavbarLeft from './navbar-left/NavbarLeft';
import Main from './main/Main';
import Fade from '@material-ui/core/Fade'

// Componente que junta o Navbar com o Main

export default class MainContainer extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <Fade in={true}>
      <div {...this.props} className='main-container'>
      <NavbarLeft/>
      <Main/>
      </div>
      </Fade>
    )
  }
}
