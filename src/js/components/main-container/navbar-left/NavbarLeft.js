import React, { Component } from 'react'
import { connect } from 'react-redux'
import {changeMain} from '../../../actions/actions'

// Main da barra lateral

class NavbarLeft extends Component {

  constructor(props){
    super(props)
  }

  render() {
    return (
      <div className='navbar-left'>
        <nav>
          <div className='profile-info'>
            <div className='photo'><img src={this.props.user.img}></img></div>
            <div className='info'><span>{this.props.user.company}</span><span>{this.props.user.email}</span></div>
          </div>
          <ul>
            <li onClick={()=> this.props.changeMain(0)} className="item" id='dashBoard'><i style={{background: `url() no-repeat`, backgroundSize:'1.5em 1.5em',backgroundPosition: 'center' }}></i>DashBoard</li>
          </ul>
        </nav>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    mainPage:state.mainPage
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    changeMain:function(page){
        dispatch(changeMain(page))
    }
  } 
}


export default connect(mapStateToProps,mapDispatchToProps)(NavbarLeft);
