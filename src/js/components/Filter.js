import React from 'react'
import {filtrar} from '../actions/actions'
import { connect } from 'react-redux'
import MenuItem from '@material-ui/core/MenuItem';

// MenuItem de filtro, OLHAR MenuFilter

const Filter = (props) => {
  return (
    <MenuItem onClick={props.onClick} className='Filter'>
      {props.children}
    </MenuItem>
  )
}

const mapStateToProps = (state, ownProps) => ({
  active: ownProps.filter === state.filter
})
const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(filtrar(ownProps.filter))
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter)
