import React, { Component } from 'react'

// O container é o main wrapper de todo o dash, provavelmente o componente mais repetido.

export default class Container extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
      <div className={`${this.props.className} container`}>
       {this.props.children}
      </div>
    )
  }
}
