import React, { Component } from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import TextField from '@material-ui/core/TextField'
import Fade from '@material-ui/core/Fade'
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Title from './main-container/main/Title';
import Container from './Container';
import $ from 'jquery';
import axios from 'axios';
import {completeCad} from '../actions/actions'

// Menu de completar cadastro, quando o perfil nao tem as informaçoes completas

class CompleteCad extends Component {

  constructor(props){
    super(props)
    this.state ={
      nome:'',
      cnpj:'',
      numero:'',
      complemento:'',
      cidade:'',
      bairro:'',
      estado:'',
      razao:'',
      cep:'',
      rua:'',
      telefone:''
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange = nome => e => {
    this.setState({
      [nome]:e.target.value
    })
  }

  onBlur(e) {
    var target = this;
   

    var currentTarget = e.currentTarget;
    var cep = $(e.target).val().replace(/\D/g, '');
    setTimeout(function () {
      if (!currentTarget.contains(document.activeElement)) {
        if (cep != "") {
          axios.get(`https://viacep.com.br/ws/${cep}/json`)
            .then(res => res.data)
            .then(data =>
              {
              target.setState({
                rua: data.logradouro,
                bairro: data.bairro,
                estado: data.uf,
                cidade: data.localidade
              })}

            )
        }
      }
    }, 0);
  }

  completar(){

    
    this.props.completeCad({
      "nome":this.state.nome,
      "CNPJ":this.state.cnpj,
      "telefone": this.state.telefone,
      "razaoSocial": this.state.razao,
      "cep": this.state.cep,
      "estado": this.state.estado,
      "cidade":this.state.cidade,
      "bairro":this.state.bairro,
      "rua":this.state.rua,
      "numero": this.state.numero,
      "complemento":this.state.complemento
    },this.props.token)
  }

  render() {

    
    return (
    <Fade in={true}>
      <div className="complete-cadastro">
      <Paper className="paper">
      <Container>
      <Title name="Para continuar você precisa completar o cadastro abaixo" />
      <form noValidate autoComplete="off">
        <div className="form">
          <div className="main-info">
          <Title name="Informações da Empresa"/>
            <TextField
            style={{ width: '200px' }}
              id="nome"
              label="Nome"
              onChange={this.handleChange('nome')}
              margin="normal"
              className="input"
              value={this.state.nome}
            />
            <TextField
              id="cnpj"
              label="CNPJ"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              onChange={this.handleChange('cnpj')}
              value={this.state.cnpj}
            >
            </TextField>
            <TextField
              id="telefone"
              label="Telefone"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              margin="normal"
              onChange={this.handleChange('telefone')}
              value={this.state.telefone}

            >
            </TextField>
            <TextField
            style={{ width: '200px' }}
              id="razao"
              label="Razão Social"
              onChange={this.handleChange('razao')}
              margin="normal"
              className="input"
              value={this.state.razao}
            />

          </div>
          <div className="endereco">
            <Title name="Endereço"/>
            <TextField
     
              onBlur={this.onBlur.bind(this)}
              onChange={this.handleChange('cep')}
              disabled={this.state.isSame}
              id="cep"
              label="Cep"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.cep}
      
            />
            <TextField
     
              disabled={this.state.isSame}
              id="rua"
              label="Rua"
              onChange={this.handleChange('rua')}
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.rua}

            /><TextField
              autoComplete='nope'
              disabled={this.state.isSame}
              onChange={this.handleChange('bairro')}
              id="bairro"
              label="Bairro"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.bairro}
         
            /> <TextField
              autoComplete='nope'
              disabled={this.state.isSame}
              onChange={this.handleChange('numero')}
              id="numero"
              label="numero"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.numero}
            />
            <TextField
              autoComplete='nope'
              disabled={this.state.isSame}
              onChange={this.handleChange('cidade')}
              id="cidade"
              label="Cidade"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.cidade}
            />
            <TextField
              autoComplete='nope'
              disabled={this.state.isSame}
              onChange={this.handleChange('estado')}
              id="uf"
              label="UF"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.estado}


            />
            <TextField
              autoComplete='nope'
              onChange={this.handleChange('complemento')}
              disabled={this.state.isSame}
              id="complemento"
              label="Complemento"
              style={{ width: '200px' }}
              margin="normal"
              className="input"
              value={this.state.complemento}

            />
          </div>
        </div>
      </form>
      <Button className="button" onClick={this.completar.bind(this)} color="primary">Enviar</Button>
    </Container>
        </Paper>
      </div>
      </Fade>
    )
  }
}
const mapStateToProps = (state)=>{
return{
  token:state.user.token
}
}
const mapDispatchToProps = (dispatch)=>{
return{
  completeCad:bindActionCreators(completeCad,dispatch)
}
}

export default connect(mapStateToProps,mapDispatchToProps)(CompleteCad)