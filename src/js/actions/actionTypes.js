//Aqui estao organizados TODOS OS TYPES!!!

export const IMPORT = 'IMPORT';
export const CHANGE_MAIN = 'CHANGE_MAIN';
export const LOADED = 'LOADED';
export const TOGGLE_FAV = 'TOGGLE_FAV';
export const SHOW_VAGA = 'SHOW_VAGA';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
export const NEW_VAGA = 'NEW_VAGA';
export const LOGGED = 'LOGGED';
export const LOGGED_BUT = 'LOGGED_BUT';
export const REQUESTING = 'REQUESTING';
export const DELETE_FILTER = 'DELETE_FILTER';
export const GET_PAYLOAD = 'GET_PAYLOAD';
export const CADASTRO_FORM = 'CADASTRO_FORM';
export const CADASTRAR = 'CADASTRAR';
export const LOGIN_FORM = 'LOGIN_FORM';
export const GET_VAGAS = 'GET_VAGAS';
export const GET_INFO_VAGAS = 'GET_INFO_VAGAS';
export const SEND_NEW_VAGA = 'SEND_NEW_VAGA';
export const FINALIZAR_VAGA = 'FINALIZAR_VAGA';
export const COMPLETE_CAD = 'COMPLETE_CAD';
export const CRIANDO_VAGA = 'CRIANDO_VAGA';
export const VAGA_CRIADA = 'VAGA_CRIADA';
export const UPDATE_VAGAS = 'UPDATE_VAGAS';
export const LOGOUT = 'LOGOUT';
export const EDIT_VAGA = 'EDIT_VAGA';
export const SHOW_CAND = 'SHOW_CAND';
export const LIST_CAND = 'LIST_CAND';

export const filtro = {
    SHOW_ALL:'SHOW_ALL',
    SHOW_FAV:'SHOW_FILTERED'
}