// Neste importasse TODOS OS TYPES como types
import * as types from './actionTypes'
import axios from 'axios'

var headers = {}

const base = 'https://beejobs-apiv2-teste.appspot.com/api'


// comando para mudar de pagina, apesar de estar sendo descontinuado desta forma
export const changeMain = (page) => {

    return page === 1
        ? (dispatch) => {
            axios.get(`${base}/listarVagasAbertas`, { headers }).then(res => {
                console.log(res),
                    dispatch({
                        type: types.GET_VAGAS,
                        payload: res.data
                    }),
                    dispatch({
                        type: types.CHANGE_MAIN,
                        page
                    })
            }
            )
        }
        : {
            type: types.CHANGE_MAIN,
            page
        }
}

//comando para indicar que está logado

export const loaded = () => {
    return {
        type: types.LOADED
    }
}

// Mecanismo de toggle do favorito ainda nao implementado com a API
export const toggleFav = (id) => {
    return {
        type: types.TOGGLE_FAV,
        id
    }
}

// Mostra a vaga, recebendo o id dela

export const showVaga = (id) => {
    return {
        type: types.SHOW_VAGA,
        id
    }
}

// Olhar melhor o InfoVagas para ter melhor mas informaçoes, borem aqui há o controle de filtros.
export const filtrar = filter => ({
    type: 'SET_VISIBILITY_FILTER',
    filter
})

//Recebe os cargos disponiveis ao clicar em criar uma nova vaga
export const createVaga = () => {
    return (dispatch) => {
        axios.get(`${base}/cargo`, { headers }).then(res => {
            console.log(res),
                dispatch({
                    type: types.NEW_VAGA
                })
        }
        )
    }
}

// Permite a abertura do form de cadastro, variando entre true e false
export const cadastroForm = () => {

    return (dispatch) => {
        dispatch({
            type: types.CADASTRO_FORM
        })
    }
}

// Açao efetivamente que cria a vaga
export const cadastrar = (infoClient) => {
    console.log(infoClient);

    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.post(`${base}/cadastrarEmpresa`,
            infoClient)
            .then(res => console.log(res))
            .catch(error => console.log(error)
            )
    }
}

// Abre o login form: varia entre true ou falso
export const loginForm = () => {

    return (dispatch) => {
        dispatch({
            type: types.LOGIN_FORM
        })
    }
}

// Envia se o usuário está logado ou nao
export const logged = (log,callback) => {
    console.log(log);

    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.post(`${base}/loginEmpresa`, log)
            .then(res => {
                console.log(res),
                
                !res.data.empresa.CNPJ
                    ? dispatch({
                        type: types.LOGGED_BUT,
                        payload: res.data
                    })
                    : dispatch({
                        type: types.LOGGED,
                        payload: res.data
                    })
                    
            }
            )
            .then(()=>{
                callback()
            })
            .catch(error => console.log(error)
            )
    }
}
export const completeCad = (info,token) => {
    headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    console.log(headers);
    
    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.post(`${base}/primeiroForm`, info ,{headers})
            .then(res => {
                console.log(res),
                         dispatch({
                        type: types.COMPLETE_CAD,
                        info
                    })
            }
            )
            .catch(error => console.log(error)
            )
    }
}

export const getInfoCompany = (token) => {
    headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.get(`${base}/listarVagasAbertas`, { headers }).then(res => {
            console.log(res),
                dispatch({
                    type: types.GET_VAGAS,
                    payload: res.data
                })
        }
        )
    }
}
export const stayLogged = (token,callback) => {
    headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    return (dispatch) => {
        axios.get(`${base}/dashEmpresa`, { headers }).then(res => {
            console.log(res),
                dispatch({
                    type: types.LOGGED,
                    payload: res.data
                }),
                callback()
        }
        )
    }
}
export const logout = () => {
    return {
        type:types.LOGOUT
    }
}
export const updatingVagas = () => {
    return (dispatch) => {
        axios.get(`${base}/listarVagasAbertas`, { headers }).then(res => {
            console.log(res),
                dispatch({
                    type: types.UPDATE_VAGAS,
                    payload: res.data
                })
        }
        )
    }
}


export const upInfoCompany = () => {
    return (dispatch) => {
        axios.get(`${base}/listarVagasAbertas`, { headers }).then(res => {
            console.log(res),
                dispatch({
                    type: types.GET_VAGAS,
                    payload: res.data
                })
        }
        )
    }
}
export const finalizarVaga = (id, callback) => {
    return (dispatch) => {
        axios.delete(`${base}/vaga/${id}`, { headers }).then(res => {
            callback()
        })
    }
}

export const getInfoVaga = (id) => {
    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.get(`${base}/listarCandidatosVaga/${id}`, { headers }).then(res => {
            dispatch({
                type: types.GET_INFO_VAGAS,
                payload: res.data.usuarios
            }),
                dispatch({
                    type: types.GET_PAYLOAD
                })
        }
        )
    }
}
export const showCand = (id,dist) => {
    console.log(headers);
    
    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.get(`${base}/vizualizarUsuario/${id}/${dist}`, { headers }).then(res => {
            console.log(res),    
                dispatch({
                    type: types.SHOW_CAND,
                    payload: res.data
                })
        }
        )
    }
}
export const listCand = () => {
    console.log(headers);
    
    return (dispatch) => {
        dispatch({
            type: types.REQUESTING
        })
        axios.get(`${base}/listarUltimasCandidaturas/`, { headers }).then(res => { 
            console.log(res);
                     
                dispatch({
                    type: types.LIST_CAND,
                    payload: res.data
                })
        }
        )
    }
}
export const concEdit = (info,id,callback) => {
    console.log(headers);
    
    return (dispatch) => {
        dispatch({
            type: types.CRIANDO_VAGA
        })
        axios.put(`${base}/vaga/${id}/`,{info}, { headers }).then(res => { 
            console.log(res),
            
            dispatch({
                type: types.VAGA_CRIADA
            }),
            callback()
        }
        )
    }
}


export const sendNewVagas = (infoVaga,callback) => {
    return (dispatch) => {
        dispatch({
            type: types.CRIANDO_VAGA
        })
        axios.post(`${base}/vaga`, infoVaga, { headers })
            .then(res =>
                dispatch({
                    type: types.VAGA_CRIADA
                })
            )
            .then(res=>callback())
            .catch(error => console.log(error),
            dispatch({
                type: types.VAGA_CRIADA
            }))
    }
}

export const requesting = () => {
    return {
        type: types.REQUESTING
    }
}
export const editVaga = (id) => {
    return {
        type: types.EDIT_VAGA,
        id
    }
}

export const deleteFilter = (category) => {
    return {
        type: types.DELETE_FILTER,
        category
    }
}

export const loggedBut = () => {
    return (dispatch) => {
        dispatch({
            type: types.LOGGED_BUT
        })
        setTimeout(() =>
            dispatch({
                type: types.LOADED
            }), 2000)
    }
}