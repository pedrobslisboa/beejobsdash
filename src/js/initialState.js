import {filtro} from './actions/actionTypes'

const initialState = {
  // abre cadastro
    cadastrar:false,
     // usado para informar que requisiçoes estao sendo feitas
    "request":false,
    // True casa o usuario esteja logado
    "logged":false,
    // Tudo foi carregado ao inicio? Isso verifica
    "isLoaded": false,
    // Apesar de estar sendo descontinuado, isso escolho a mainPage
    "mainPage": 0,
    // Se clicar para ver todas as vagas vai mudar aqui
    "isVagaShow": false,
    // Se clicar para criar vaga vai mudar aqui
    "newVaga":false,
    // Se clicar para ver o candidato, vai mudar aqui
    "showCand":false,
     // Se clicar para ver alguma vaga, vai mudar aqui
    "vagaOn":"",
      // Se clicar para editar vaga, vai mudar aqui
    "editVaga":false,
     // Recebe os dados da vaga a ser editada
    "vagaEditar":{},
     // Categorias de filtro
    "category":[],
     // Se está ou nao filtrando
    "filter":"SHOW_ALL",
     // Tem todos as infos ao logar? Se nao vai ser true e vai enviar o usuario
     // para completar cadastro
    "hasAttr":null,
     // Aqui vao as infos do usuario
    "user": {
      "_id": "5b7b0df4eaf893ecaed025ee",
      "company": "SEQUITUR",
      "email": "",
      "isActive": true,
      "img": "https://via.placeholder.com/200x200"
    },
         // Aqui vao as infos do candidato
    "candidatos": [
      {
        "vaga": {
          "_id": "5b7c0e12aa65d5e55d6da199",
          "nome": "Assistente"
        },
        "_id": "5b7c0e125874ffe0f67661c2",
        "bairro":"jacarepagua",
        "cidade":"rio de janeiro",
        "nome": "Savage Anthony",
        "formacao": "Superior Completo",
        "curriculo": "link aleatório",
        "isFav": false
      },
      {
        "vaga": {
          "_id": "5b7c0e1221991ecca75f523c",
          "nome": "Vigilante"
        },
        "_id": "5b7c0e125d565dbe3bff1745",
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "nome": "Vera Vance",
        "formacao": "Superior Cursando",
        "curriculo": "link aleatório",
        "isFav": false
      },
      {
        "vaga": {
          "_id": "5b7c0e1221991ecca75f523c",
          "nome": "Vigilante"
        },
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "_id": "5b7c0e129ec15ff67e3977c3",
        "nome": "Lou Horne",
        "formacao": "Superior Completo",
        "curriculo": "link aleatório",
        "isFav": false
      },
      {
        "vaga": {
          "_id": "5b7c0e12aa65d5e55d6da199",
          "nome": "Assistente"
        },
        "_id": "5b7c0e12fac8471b28f94a8a",
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "nome": "Hardy Doyle",
        "formacao": "Superior Completo",
        "curriculo": "link aleatório",
        "isFav": false
      },
      {
        "vaga": {
          "_id": "5b7c0e12365c2fa363539fee",
          "nome": "Vendedor"
        },
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "_id": "5b7c0e123c18798d4b837a4c",
        "nome": "Morrow Fitzpatrick",
        "formacao": "Superior Completo",
        "curriculo": "link aleatório",
        "isFav": true
      },
      {
        "vaga": {
          "_id": "5b7c0e12365c2fa363539fee",
          "nome": "Vendedor"
        },
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "_id": "5b7c0e1289f863d221e92033",
        "nome": "Cameron Malone",
        "formacao": "Médio Completo",
        "curriculo": "link aleatório",
        "isFav": false
      },
      {
        "vaga": {
          "_id": "5b7c0e12aa65d5e55d6da199",
          "nome": "Assistente"
        },
        "bairro":"barra",
        "cidade":"rio de janeiro",
        "_id": "5b7c0e12f721fbff23ae09e7",
        "nome": "Bender Steele",
        "formacao": "Superior Completo",
        "curriculo": "link aleatório",
        "isFav": false
      }
    ],
    // Aqui vao as infos do candidato
    "vagas":[],
    // Aqui vao as da vaga aberta
    "candidatosVagaOn":[],
    // Diz se está criando uma vaga ou nao
    "criandoNovaVaga":false,

  }
  

export default initialState;